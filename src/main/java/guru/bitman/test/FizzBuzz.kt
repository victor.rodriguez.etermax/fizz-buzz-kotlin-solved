package guru.bitman.test

class FizzBuzz {
	fun run(): List<String> {
		return (1..100).map { ToString(it) }
	}

	private fun ToString(i: Int): String =
		FizzBuzzConverter().convert(i)
}
