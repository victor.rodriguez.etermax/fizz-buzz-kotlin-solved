package guru.bitman.test

class FizzBuzzConverter {


	fun convert(i: Int): String {
		if (i % 3 == 0 && i % 5 == 0) {
			return "FizzBuzz"
		}
		if (i % 3 == 0 && i % 5 != 0) {
			return "Fizz"
		}
		if (i % 3 != 0 && i % 5 == 0) {
			return "Buzz"
		}
		return i.toString()
	}




}
